# Fabrication file

This directory contains the fabrication files used in the project namely:

- Gerber files
- Bill of Materials file

The used PCB fabrication house is [SeeedStudio](https://www.seeedstudio.com/). Check them out.

I bought the materials over at [LCSC](https://lcsc.com/) and soldered them myself.
